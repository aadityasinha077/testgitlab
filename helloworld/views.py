from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
def Index(request):
    return HttpResponse('<h1>Git Lab test is successful.</h1>')

def Welcome(request):
    return HttpResponse('<h1>Welcome to Git Lab.</h1>')